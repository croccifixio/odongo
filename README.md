#Odongo.xyz

This is the source code for [odongo.xyz][1] which was built using [Middleman][2].

Feel free to send a [pull request][3] or open an [issue][4] if you notice any typos, wonky rendering issues, etc.

#License

Copyright &copy; 2016-2017 Emmanuel Odongo. See [LICENSE][5] for details.

[1]: http://odongo.xyz
[2]: https://middlemanapp.com
[3]: https://github.com/Croccifixio/blog/pull/new/master
[4]: https://github.com/Croccifixio/blog/issues/new
[5]: https://github.com/Croccifixio/blog/blob/master/LICENSE.md
