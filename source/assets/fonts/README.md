The font files have been removed from the GitHub repository due to licensing concerns.

The licenses purchased for the fonts used on this site do not permit the distributuin of the font files unless as part of a client's project.